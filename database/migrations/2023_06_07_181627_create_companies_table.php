<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string("name", 150);
            $table->string("phone", 100);
            $table->string("timezone")->nullable();
            $table->string("web", 150)->nullable();
            $table->tinyInteger("enabled")->nullable();
            $table->string("update_by")->nullable();
            $table->string("created_by")->nullable();
            $table->string("deleted_by")->nullable();
            $table->timestamps();
        });
        //$table->string("name", 150);
        //$table->foreignId("department_id")->constrained("departments")->onUpdate("cascade")->onDelete("restrict");

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('companies');
    }
};
