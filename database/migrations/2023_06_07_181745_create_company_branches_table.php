<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('company_branches', function (Blueprint $table) {
            $table->id();
            $table->string("address", 150)->nullable();
            $table->string("contact_email", 100)->nullable();
            $table->string("contact_name", 100)->nullable();
            $table->string("contact_phone", 100)->nullable();
            $table->string("country", 50)->nullable();
            $table->string("default_lang", 20)->nullable();
            $table->string("email", 100)->nullable();
            $table->tinyInteger("enabled")->nullable();
            $table->string("legal_name", 100)->nullable();
            $table->string("name", 100)->nullable();
            $table->string("phone", 100)->nullable();
            $table->string("rfc", 100)->nullable();
            $table->string("timezone", 100)->nullable();
            $table->string("web", 100)->nullable();
            $table->string("update_by")->nullable();
            $table->string("created_by")->nullable();
            $table->string("deleted_by")->nullable();
            $table->foreignId("id_company")->constrained("companies")->onUpdate("cascade")->onDelete("restrict");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('company_branches');
    }
};
