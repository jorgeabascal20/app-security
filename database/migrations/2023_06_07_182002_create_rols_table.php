<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rols', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger("company_filter")->nullable();
            $table->tinyInteger("department_filter")->nullable();
            $table->string("description", 500)->nullable();
            $table->string("name", 100)->nullable();
            $table->tinyInteger("enabled")->nullable();
            $table->string("update_by")->nullable();
            $table->string("created_by")->nullable();
            $table->string("deleted_by")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rols');
    }
};
