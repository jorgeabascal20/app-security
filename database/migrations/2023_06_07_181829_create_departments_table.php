<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger("enabled")->nullable();
            $table->string("name", 100)->nullable();
            $table->string("update_by")->nullable();
            $table->string("created_by")->nullable();
            $table->string("deleted_by")->nullable();
            $table->foreignId("id_company")->constrained("companies")->onUpdate("cascade")->onDelete("restrict");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('departments');
    }
};
