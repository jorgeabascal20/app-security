<?php

namespace App\Http\Controllers;

use App\Models\Company_branch;
use Illuminate\Http\Request;

class CompanyBranchController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Company_branch $company_branch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Company_branch $company_branch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Company_branch $company_branch)
    {
        //
    }
}
