<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'updated_by',
        'created_by',
        'deleted_by',
        'code',
        'description',
        'disabled'
    ];


}
