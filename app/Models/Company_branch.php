<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company_branch extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'phone',
        'timezone',
        'email',
        'web',
        'enabled',
        'address',
        'updated_by',
        'created_by',
        'deleted_by',
        'id_company',
    ];

}
